# Overview

The idea around this project is to create a mail plugin that uses a more vanilla-like approach. The mail-box is a chest, trapped chest, double chest, barrel, hopper, dropper, or dispenser.

## How it works

The plugin should find a sign, upon creation, that the first line is written with [mail], not case sensitive. The second line tells the plugin who it belongs to. If a player isn't an admin the name should auto-populate with the name of the player.

The data could either be handled with a database or file system. A file system is preferred by the user group. The file system should handle the name of the player, the position of the chest.

All items being sent from another chest, using [send] (not case sensitive), should send it contents, by adding, into the chest to the appropriate name attached to it.