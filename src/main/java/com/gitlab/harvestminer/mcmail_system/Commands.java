package com.gitlab.harvestminer.mcmail_system;

import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import com.gitlab.harvestminer.mcmail_system.util.MailBox;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor
{
    public String cmd1 = "mail";

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (sender instanceof Player)
        {
            Player player = (Player) sender;

            if (cmd.getName().equalsIgnoreCase(cmd1))
            {
                if (FileHandler.CheckForFile(MailBox.GetFileLocation(player.getName())))
                {
                    if (MailBox.DoesMailBoxBlockExist(player.getName()))
                    {
                        if (!MailBox.IsMailboxEmpty(player.getName()))
                        {
                            String name = Config.HasMailTxt.replace("{playerName}", player.getName());
                            player.sendMessage(ChatColor.valueOf(Config.HasMailColor) + name);
                        } else
                        {
                            String name = Config.NoMailTxt.replace("{playerName}", player.getName());
                            player.sendMessage(ChatColor.valueOf(Config.NoMailColor) + name);
                        }

                        return true;
                    }
                }
            }
        }

        return false;
    }
}
