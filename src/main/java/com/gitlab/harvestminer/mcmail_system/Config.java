package com.gitlab.harvestminer.mcmail_system;

import org.bukkit.plugin.Plugin;

public class Config
{
    public static Plugin plugin = MCMail_System.getPlugin(MCMail_System.class);
    public static String HasMailTxt;
    public static String HasMailColor;
    public static String NoMailTxt;
    public static String NoMailColor;

    public static String getText(String path) { return plugin.getConfig().getString(path); }
    public static String getColor(String path) { return plugin.getConfig().getString(path); }

    public static void populateConfig ()
    {
        HasMailTxt = getText("Text.Has_Mail.Text");
        HasMailColor = getColor("Text.Has_Mail.Color");
        NoMailTxt = getText("Text.No_Mail.Text");
        NoMailColor = getColor("Text.No_Mail.Color");
    }
}
