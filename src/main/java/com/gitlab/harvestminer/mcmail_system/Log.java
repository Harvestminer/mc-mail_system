package com.gitlab.harvestminer.mcmail_system;

import com.gitlab.harvestminer.mcmail_system.filing_system.FileData;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log
{
    public static void CreateLog(String log, String playerName, String recipient)
    {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-YYYY HH:mm:ss");

        FileData file = new FileData();

        file.AddToLogs();
        file.SetMsg("[" + formatter.format(calendar.getTime()) + "] " + playerName + " sends item(s) to " + recipient + ": " + log);

        FileHandler.AddFileData(file);
    }
}
