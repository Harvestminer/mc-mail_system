package com.gitlab.harvestminer.mcmail_system;

import com.gitlab.harvestminer.mcmail_system.event_system.DestroySign;
import com.gitlab.harvestminer.mcmail_system.event_system.OnEffectSign;
import com.gitlab.harvestminer.mcmail_system.event_system.OnInteractInventory;
import com.gitlab.harvestminer.mcmail_system.event_system.OnPlayerJoin;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import com.gitlab.harvestminer.mcmail_system.util.GrabServer;
import com.gitlab.harvestminer.mcmail_system.util.MailBox;
import org.bukkit.plugin.java.JavaPlugin;

public final class MCMail_System extends JavaPlugin
{
    @Override
    public void onEnable()
    {
        FileHandler.AddDirectories();
        saveDefaultConfig();

        new OnEffectSign(this);
        new OnPlayerJoin(this);
        new DestroySign(this);
        new OnInteractInventory(this);

        GrabServer.SetServer(getServer());

        this.getLogger().info("Mail System Plugin Enabled");

        Config.populateConfig();

        this.getCommand("mail").setExecutor(new Commands());
    }

    @Override
    public void onDisable()
    {

    }
}
