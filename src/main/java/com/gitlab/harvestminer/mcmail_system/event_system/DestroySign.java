package com.gitlab.harvestminer.mcmail_system.event_system;

import com.gitlab.harvestminer.mcmail_system.MCMail_System;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileData;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import com.gitlab.harvestminer.mcmail_system.util.BlocksAround;
import com.gitlab.harvestminer.mcmail_system.util.MailBox;
import com.gitlab.harvestminer.mcmail_system.util.PlayerPerms;
import com.gitlab.harvestminer.mcmail_system.util.Signs;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class DestroySign implements Listener
{
    public DestroySign(MCMail_System plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void OnPlayerDestroy(BlockBreakEvent event)
    {
        Block block = BlocksAround.HasSignAround(event.getBlock().getLocation());
        Player player = event.getPlayer();

        if (block.getState().getBlockData().getMaterial() != event.getBlock().getState().getBlockData().getMaterial())
        {
            Sign sign = (Sign) block.getState();

            if (MailBox.IsCurrentMailBox(event.getBlock().getLocation(), sign.getLine(1)))
            {
                if (Signs.IsTaggedMail(sign))
                {
                    if (!PlayerPerms.CanDestroySign(player) && !player.getName().equals(sign.getLine(1)))
                    {
                        event.setCancelled(true);
                    } else
                    {
                        FileData file = new FileData();

                        file.AddMailBox(sign.getLine(1), player);
                        FileHandler.DeleteFile(file);
                    }
                } else if (Signs.IsTaggedSend(sign))
                {
                    if (!PlayerPerms.CanDestroySign(player) && !player.getName().equals(sign.getLine(1)))
                    {
                        event.setCancelled(true);
                    }
                }
            }
        }
        else if (Signs.IsWallSign(event.getBlock()))
        {
            Sign sign = (Sign) event.getBlock().getState();

            if (MailBox.IsCurrentMailBox(event.getBlock().getLocation(), sign.getLine(1)))
            {
                if (Signs.IsTaggedMail(sign))
                {
                    if (!PlayerPerms.CanDestroySign(player) && !player.getName().equals(sign.getLine(1)))
                    {
                        event.setCancelled(true);
                    } else
                    {
                        FileData file = new FileData();

                        file.AddMailBox(sign.getLine(1), player);
                        FileHandler.DeleteFile(file);
                    }
                } else if (Signs.IsTaggedSend(sign))
                {
                    if (!PlayerPerms.CanDestroySign(player) && !player.getName().equals(sign.getLine(1)))
                    {
                        event.setCancelled(true);
                    }
                }
            }
        }
    }

    @EventHandler
    public void OnExplosionDestroy(EntityExplodeEvent event)
    {
        for (int index = 0; index < event.blockList().size(); index++)
        {
            if (Signs.IsWallSign(event.blockList().get(index)))
            {
                Sign sign = (Sign) event.blockList().get(index).getState();

                if (Signs.IsTaggedMail(sign) || Signs.IsTaggedSend(sign))
                {
                    event.setCancelled(true);
                }
            }
        }
    }
}
