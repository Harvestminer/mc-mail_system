package com.gitlab.harvestminer.mcmail_system.event_system;

import com.gitlab.harvestminer.mcmail_system.MCMail_System;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileData;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import com.gitlab.harvestminer.mcmail_system.util.PlayerPerms;
import com.gitlab.harvestminer.mcmail_system.util.Signs;
import com.gitlab.harvestminer.mcmail_system.util.SupportedInventories;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SignChangeEvent;

public class OnEffectSign implements Listener
{
    public OnEffectSign(MCMail_System plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    // Move non-essential event methods to another file

    @EventHandler
    private void _signPlaced(SignChangeEvent event)
    {
        Sign sign = (Sign) event.getBlock().getState();
        Player player = event.getPlayer();
        Block block = event.getBlock();

        if (Signs.IsWallSign(block))
        {
            switch (checkFirstLine(event))
            {
                case "mail":
                    if (getInventoryBlock(sign) != sign.getBlock())
                    {
                        MailSign(event, sign, player);
                    }
                    break;
                case "send":
                    if (getInventoryBlock(sign) != sign.getBlock())
                    {
                        SendSign(event, player);
                    }
                    break;
                case "":
                    break;
            }
        }
    }

    private void MailSign(SignChangeEvent event, Sign sign, Player player)
    {
        FileData file = new FileData();

        if (!PlayerPerms.CanPlaceSign(player))
        {
            event.setLine(1, player.getName());
            file.AddMailBox(player.getName(), player);
        }
        else
        {
            file.AddMailBox(event.getLine(1), player);
        }

        file.SetPosition(new int[] {
                getInventoryBlock(sign).getX(),
                getInventoryBlock(sign).getY(),
                getInventoryBlock(sign).getZ()
        });

        FileHandler.AddFileData(file);
    }

    private void SendSign(SignChangeEvent event, Player player)
    {
        if (!PlayerPerms.CanPlaceSign(player))
        {
            event.setLine(1, player.getName());
        }
    }

    private Block getInventoryBlock(Sign sign)
    {
        BlockData data = sign.getBlockData();

        if (data instanceof Directional)
        {
            Directional directional = (Directional) data;
            Block blockBehind = sign.getBlock().getRelative(directional.getFacing().getOppositeFace());

            for(Material material : SupportedInventories.Inventories)
            {
                if (material == blockBehind.getBlockData().getMaterial())
                {
                    return blockBehind;
                }
            }

            return sign.getBlock();
        }

        return sign.getBlock();
    }

    private String checkFirstLine(SignChangeEvent sign)
    {
        if (sign.getLine(0).equalsIgnoreCase("[mail]"))
        {
            return "mail";
        }
        else if (sign.getLine(0).equalsIgnoreCase("[send]"))
        {
            return "send";
        }

        return "";
    }
}
