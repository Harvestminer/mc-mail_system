package com.gitlab.harvestminer.mcmail_system.event_system;

import com.gitlab.harvestminer.mcmail_system.Log;
import com.gitlab.harvestminer.mcmail_system.MCMail_System;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileData;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import com.gitlab.harvestminer.mcmail_system.util.BlocksAround;
import com.gitlab.harvestminer.mcmail_system.util.GrabServer;
import com.gitlab.harvestminer.mcmail_system.util.MailBox;
import com.gitlab.harvestminer.mcmail_system.util.SupportedInventories;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;

public class OnInteractInventory implements Listener
{
    public OnInteractInventory(MCMail_System plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void Interaction(InventoryCloseEvent event)
    {
        StringBuilder LogMsg = new StringBuilder(" ");

        for (Material material : SupportedInventories.Inventories)
        {
            Block block = event.getInventory().getLocation().getBlock();

            if (block.getState().getBlockData().getMaterial().equals(material))
            {
                Player player = (Player) event.getPlayer();
                Location loc = block.getLocation();
                Block _block = BlocksAround.HasSignAround(loc);

                if (_block != block)
                {
                    Sign sign = (Sign) _block.getState();
                    String prefix = sign.getLine(0);

                    if (prefix.equalsIgnoreCase("[send]"))
                    {
                        Inventory inventory = event.getInventory();

                        ItemStack[] items = inventory.getContents();

                        FileData file = new FileData();
                        file.AddMailBox(sign.getLine(1), player);

                        if (FileHandler.CheckForFile(file.GetLocation()) && MailBox.DoesMailBoxBlockExist(sign.getLine(1)))
                        {
                            FileData fileData = FileHandler.ReadFile(MailBox.GetFileLocation(sign.getLine(1)));

                            int[] position = fileData.GetPosition();
                            Location location = new Location(GrabServer.GetServer().getWorld(fileData.GetWorld()), position[0], position[1], position[2]);

                            for (ItemStack item : items)
                            {
                                if (item != null)
                                {
                                    LogMsg.append(item.getI18NDisplayName()).append("*").append(item.getAmount()).append(" ");
                                    ((InventoryHolder) location.getBlock().getState()).getInventory().addItem(item);
                                }
                            }

                            Log.CreateLog(LogMsg + "\n", event.getPlayer().getName(), sign.getLine(1));
                            event.getInventory().clear();
                        }
                    }
                }
            }
        }
    }
}
