package com.gitlab.harvestminer.mcmail_system.event_system;

import com.gitlab.harvestminer.mcmail_system.Config;
import com.gitlab.harvestminer.mcmail_system.MCMail_System;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import com.gitlab.harvestminer.mcmail_system.util.MailBox;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class OnPlayerJoin implements Listener
{
    public OnPlayerJoin(MCMail_System plugin)
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void OnJoinGame(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();

        if (FileHandler.CheckForFile(MailBox.GetFileLocation(player.getName())))
        {
            if (MailBox.DoesMailBoxBlockExist(player.getName()))
            {
                if (!MailBox.IsMailboxEmpty(player.getName()))
                {
                    String name = Config.HasMailTxt.replace("{playerName}", player.getName());
                    player.sendMessage(ChatColor.valueOf(Config.HasMailColor) + name);
                }
            }
        }
    }
}
