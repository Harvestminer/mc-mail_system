package com.gitlab.harvestminer.mcmail_system.filing_system;

import org.bukkit.entity.Player;
import java.io.File;

public class FileData
{
    private int[] _position;

    private String _name;

    private String _location;

    private String _msg;

    private String _world;

    public String GetWorld()
    {
        return _world;
    }

    public void SetWorld(String world)
    {
        this._world = world;
    }

    public String[] GetLogData()
    {
        return LogData;
    }

    public void SetLogData(String[] logData)
    {
        LogData = logData;
    }

    private String[] LogData;

    public String GetPlayerName()
    {
        return _name;
    }

    public void SetPlayerName(String name)
    {
        _name = name;
    }

    public int[] GetPosition()
    {
        return _position;
    }

    public void SetPosition(int[] position)
    {
        _position = position;
    }

    public String GetLocation() { return _location; }

    public String GetMsg() { return _msg; }

    public void SetMsg(String Msg) { _msg = Msg; }

    public void AddMailBox (String playerName, Player player)
    {
        _location = "MailBoxes" + File.separator + playerName;
        _name = playerName;
        _world = player.getWorld().getName();
    }

    public void AddToLogs ()
    {
        _location = "Logs" + File.separator + "Transaction-Log";
    }
}
