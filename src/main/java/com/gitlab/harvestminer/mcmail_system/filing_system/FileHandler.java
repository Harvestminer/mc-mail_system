package com.gitlab.harvestminer.mcmail_system.filing_system;

import com.gitlab.harvestminer.mcmail_system.MCMail_System;
import org.bukkit.plugin.Plugin;
import java.io.*;

public class FileHandler
{
    private static Plugin plugin = MCMail_System.getPlugin(MCMail_System.class);

    private static String _folder = plugin.getDataFolder().getAbsolutePath() + File.separator;

    public static void AddDirectories()
    {
        File LogDirectory = new File(_folder + "Logs");
        File MailBoxesDirectory = new File(_folder + "MailBoxes");

        try
        {
            if (!LogDirectory.exists())
            {
                LogDirectory.mkdirs();
            }
            if (!MailBoxesDirectory.exists())
            {
                MailBoxesDirectory.mkdirs();
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static boolean CheckForFile(String name)
    {
        File _file = new File(_folder + name + ".yml");

        if (_file.exists())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static void AddFileData(FileData file)
    {
        if (CheckForFile(file.GetLocation()))
        {
            if (!file.GetLocation().contains("Logs"))
            {
                DeleteFile(file);
            }
        }

        CreateFile(file);
    }

    private static void CreateFile(FileData file)
    {
        // Create file if not existing
        int[] position = file.GetPosition();
        String fileName = file.GetLocation();

        try
        {
            File _file = new File(_folder + fileName + ".yml");
            FileWriter writer = new FileWriter(_file.getPath(), true);

            if (file.GetLocation().contains("Logs"))
            {
                StringBuilder msg = new StringBuilder();

                if (CheckForFile(file.GetLocation()))
                {
                    FileData fileData = ReadFile(file.GetLocation());
                    String[] logData = fileData.GetLogData();

                    if (logData != null)
                    {
                        msg.append(file.GetMsg());
                    }
                }
                else
                {
                    msg = new StringBuilder(file.GetMsg());
                }

                DeleteFile(file);
                writer.write(msg.toString());
            }
            else
            {
                writer.write(file.GetPlayerName() + "\n" + position[0] + "," + position[1] + "," + position[2] + "\n" + file.GetWorld());
            }

            writer.close();
        }
        catch (IOException e)
        {
            System.out.println("An Error Occurred");
            e.printStackTrace();
        }
    }

    public static void DeleteFile(FileData file)
    {
        File _file = new File(_folder + file.GetLocation() + ".yml");

        if (CheckForFile(file.GetLocation()))
        {
           _file.delete();
        }
        else
        {
            System.out.println("File: " + _file.getName() + ", doesn't exist. Are you trying to delete the file twice?");
        }
    }

    public static FileData ReadFile(String name)
    {
        if (CheckForFile(name))
        {
            try
            {
                FileData fileData = new FileData();
                File file = new File(_folder + name + ".yml");

                FileReader reader = new FileReader(file);
                BufferedReader bReader = new BufferedReader(reader);

                if (name.contains("MailBoxes"))
                {
                    String line;

                    line = bReader.readLine();
                    line = bReader.readLine();

                    // If null: Cluster Fuck!

                    String lines[] = line.split(",");

                    fileData.SetPosition(new int[]{Integer.parseInt(lines[0]), Integer.parseInt(lines[1]), Integer.parseInt(lines[2])});

                    line = bReader.readLine();
                    fileData.SetWorld(line);
                }

                if (name.contains("Logs"))
                {
                    String[] lines = new String[(int)bReader.lines().count()];

                    for (int index = 0; index < (int)bReader.lines().count(); index++)
                    {
                        lines[index] = bReader.readLine();
                    }

                    fileData.SetLogData(lines);
                }

                reader.close();
                bReader.close();

                return fileData;
            }
            catch (FileNotFoundException e)
            {
                System.out.println("An Error Occurred");
                e.printStackTrace();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }

            return null;
        }

        return null;
    }
}
