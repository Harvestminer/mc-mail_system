package com.gitlab.harvestminer.mcmail_system.util;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.block.Block;

public class BlocksAround
{
    public static Block HasSignAround(Location loc)
    {
        for (int index = 0; index < GetLocAround(loc).length; index++)
        {
            for (Material material : SupportedInventories.Inventories)
            {
                if (material == loc.getBlock().getBlockData().getMaterial())
                {
                    Block _block = Bukkit.getWorld(loc.getWorld().getName()).getBlockAt(GetLocAround(loc)[index]);

                    if (Tag.WALL_SIGNS.isTagged(_block.getBlockData().getMaterial()))
                    {
                        return _block;
                    }
                }
            }
        }
        return loc.getBlock();
    }

    private static Location[] GetLocAround(Location loc)
    {
        Location[] locations = new Location[]
                {
                        loc.clone().add(0, 0, 1),
                        loc.clone().add(0, 1, 0),
                        loc.clone().add(1, 0, 0),
                        loc.clone().subtract(0, 0, 1),
                        loc.clone().subtract(0, 1, 0),
                        loc.clone().subtract(1, 0, 0)
                };
        return locations;
    }
}
