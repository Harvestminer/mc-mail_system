package com.gitlab.harvestminer.mcmail_system.util;

import org.bukkit.Server;

public class GrabServer
{
    private static Server _server;

    public static void SetServer(Server server)
    {
        _server = server;
    }

    public static Server GetServer()
    {
        return _server;
    }
}
