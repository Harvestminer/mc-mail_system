package com.gitlab.harvestminer.mcmail_system.util;

import com.gitlab.harvestminer.mcmail_system.filing_system.FileData;
import com.gitlab.harvestminer.mcmail_system.filing_system.FileHandler;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import java.io.File;

public class MailBox
{
    public static String GetFileLocation(String playerName)
    {
        return "MailBoxes" + File.separator + playerName;
    }

    public static boolean IsMailboxEmpty(String playerName)
    {
        FileData fileData = FileHandler.ReadFile(GetFileLocation(playerName));

        int[] position = fileData.GetPosition();

        Location location = new Location(GrabServer.GetServer().getWorld(fileData.GetWorld()), position[0], position[1], position[2]);

        for (Material material : SupportedInventories.Inventories)
        {
            if (material == location.getBlock().getBlockData().getMaterial())
            {
                InventoryHolder container = (InventoryHolder) location.getBlock().getState();

                for (ItemStack item : container.getInventory().getContents())
                {
                    if (item != null)
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public static boolean DoesMailBoxBlockExist(String playerName)
    {
        FileData fileData = FileHandler.ReadFile(GetFileLocation(playerName));

        int[] position = fileData.GetPosition();

        Location location = new Location(GrabServer.GetServer().getWorld(fileData.GetWorld()), position[0], position[1], position[2]);

        for (Material material : SupportedInventories.Inventories)
        {
            if (material == location.getBlock().getBlockData().getMaterial())
            {
                return true;
            }
        }

        return false;
    }

    public static boolean IsCurrentMailBox(Location currentLocation, String playerName)
    {
        FileData fileData = FileHandler.ReadFile(GetFileLocation(playerName));

        int[] position = fileData.GetPosition();

        Location location = new Location(GrabServer.GetServer().getWorld(fileData.GetWorld()), position[0], position[1], position[2]);

        return location.equals(currentLocation);
    }
}
