package com.gitlab.harvestminer.mcmail_system.util;

import org.bukkit.entity.Player;

public class PlayerPerms
{
    public static boolean CanDestroySign(Player player)
    {
        return player.hasPermission("MCMail_System.canBreakMailbox") || player.isOp();
    }

    public static boolean CanPlaceSign(Player player)
    {
        return player.hasPermission("MCMail_System.canPlaceSigns") || player.isOp();
    }

    public static boolean CanAccessAllMailBoxes(Player player)
    {
        return player.hasPermission("MCMail_System.canAccessAllMailboxes") || player.isOp();
    }
}
