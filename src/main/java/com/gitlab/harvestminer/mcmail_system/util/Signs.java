package com.gitlab.harvestminer.mcmail_system.util;

import org.bukkit.Tag;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;

public abstract class Signs
{
    public static boolean IsTaggedMail(Sign sign)
    {
        return sign.getLine(0).equalsIgnoreCase("[mail]");
    }
    public static boolean IsTaggedSend(Sign sign)
    {
        return sign.getLine(0).equalsIgnoreCase("[send]");
    }
    public static boolean IsWallSign(Block block)
    {
        return Tag.WALL_SIGNS.isTagged(block.getBlockData().getMaterial());
    }
}
