package com.gitlab.harvestminer.mcmail_system.util;

import org.bukkit.Material;

public abstract class SupportedInventories
{
    public static Material[] Inventories = new Material[]
            {
                    Material.CHEST, Material.BARREL, Material.HOPPER, Material.DISPENSER,
                    Material.DROPPER, Material.TRAPPED_CHEST
            };
}
