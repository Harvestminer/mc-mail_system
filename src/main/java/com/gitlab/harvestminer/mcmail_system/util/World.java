package com.gitlab.harvestminer.mcmail_system.util;

import org.bukkit.Bukkit;

public abstract class World
{
    public static org.bukkit.World GetWorld()
    {
        // Not an guarantee that the first item is the overworld
        return Bukkit.getWorlds().get(0);
    }
}
